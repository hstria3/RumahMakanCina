# RumahMakanCina

Aplikasi simpel menggunakan lumen dan vue.js untuk menampilkan data Rumah Makan Cina. Komponennya yaitu :
1. Menu Masakan
          
2. Pegawai
          


Your lumen project must use localhost;

Steps : 


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

# UI APLIKASI

## 1. Beranda
Ini adalah tampilan utama aplikasi, seharusnya dibuat menarik perhatian.
![Beranda](https://imgur.com/a/8Ipr9do)

## 2. Read Data (Fitur Menu Masakan)
Tampilan aplikasi yang menunjukkan list data yang ada pada database.
![ReadData](https://imgur.com/a/jjTWarI)

## 3. Create Data (Fitur Menu Masakan)
Sebuah menu untuk menambah data baru ke database.
![ReadData](https://imgur.com/a/mmDqAP9)

## 4. Edit Data (Fitur Menu Masakan)
Sebuah menu untuk mengedit data yang sudah ada di database.
![ReadData](https://imgur.com/a/rXT3RxC)

Berikut adalah penjelasan aplikasi ini. Semoga bermanfaat.