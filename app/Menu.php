<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

Class Menu extends Model
{

  public $table = 't_menu';

  protected $fillable = ['nama_masakan','harga'];

}
