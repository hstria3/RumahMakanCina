<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

Class Pegawai extends Model
{

  public $table = 't_pegawai';

  protected $fillable = ['nama_pegawai','jenis_kelamin','umur','alamat'];

}
