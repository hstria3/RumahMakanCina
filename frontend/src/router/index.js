import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Pegawai from '@/components/Pegawai'
import PegawaiForm from '@/components/PegawaiForm'
import Menu from '@/components/Menu'
import MenuForm from '@/components/MenuForm'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/pegawai',
      name: 'Pegawai',
      component: Pegawai
    },
    {
      path: '/pegawai/create',
      name: 'PegawaiCreate',
      component: PegawaiForm
    },
    {
      path: '/pegawai/:id',
      name: 'PegawaiEdit',
      component: PegawaiForm
    },
    {
      path: '/menu',
      name: 'Menu',
      component: Menu
    },
    {
      path: '/menu/create',
      name: 'MenuCreate',
      component: MenuForm
    },
    {
      path: '/menu/:id',
      name: 'MenuEdit',
      component: MenuForm
    }
  ]
})
