<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//Menu
$router->post('/menu','MenuController@create');
$router->get('/menu','MenuController@read');
$router->post('/menu/{id}','MenuController@update');
$router->delete('/menu/{id}','MenuController@delete');
$router->get('menu/{id}','MenuController@detail');

//Pegawai
$router->post('/pegawai','PegawaiController@create');
$router->get('/pegawai','PegawaiController@read');
$router->post('/pegawai/{id}','PegawaiController@update');
$router->delete('/pegawai/{id}','PegawaiController@delete');
$router->get('pegawai/{id}','PegawaiController@detail');
